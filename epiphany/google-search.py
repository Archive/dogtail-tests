#!/usr/bin/env python
# Dogtail demo script
__author__ = 'David Malcolm <dmalcolm@redhat.com>'

# Test of filling in a form in a web browser
# Under construction.  Doesn't yet work

from dogtail import *
from dogtail.tree import *
from dogtail.distro import *

class EpiphanyApp(Application):
    """Utility wrapper for Epiphany"""

    def __init__(self):
        Application.__init__(self, root.application("epiphany"))

        if isinstance(distro, Debian):
            self.epiPackageName="epiphany-browser"
        else:
            self.epiPackageName="epiphany"
        self.epiVersion = packageDb.getVersion(self.epiPackageName)
        print "Epiphany version %s"%self.epiVersion

    def browseToUrl(self, urlString):
        # Click on File->New Tab on some epiphany window:
        newTabMenuItem = self.menu("File").menuItem("New Tab")
        newTabMenuItem.click()

        window = EpiphanyWindow(newTabMenuItem.findAncestor(predicate.IsAWindow()))

        tabs = window.tabs()

        # Set URL:
        print window.urlEntry().extents
        window.urlEntry().text = urlString
        window.urlEntry().doAction('activate')

        # This is in the final tab; return it:
        return tabs[-1]

class EpiphanyWindow(Window):
    def __init__(self, node):
        Window.__init__(self, node)
        self.pageTabList = self.child(roleName='page tab list', debugName='Page Tab List')

    def tabs(self):
        """
        FIXME: not true: Get all tabs of this window as a list of EpiphanyTab instances
        """
        return self.pageTabList.findChildren (predicate.GenericPredicate(roleName='page tab'), recursive=True)

    def urlEntry(self):
        """
        Get the text entry Node for entering URLs.
        FIXME: this is currently something of a hack
        """
        # FIXME: we hope that this gives us the correct text entry:
        return self.child(roleName='text', debugName='URL Entry')

class GoogleFrontPage(Node):
    def __init__(self, node):
        Node.__init__(self, node)
        self.searchButton = self.button('Google Search')
        self.imFeelingLuckyButton = self.button("I'm Feeling Lucky")

        # Locate the text entry dialog as a sibling of the search button
        self.textEntry = self.searchButton.parent.child(roleName='text', debugName='Search String Text Entry')

import dogtail.config
dogtail.config.config.debugSearching=True

# Epiphany doesn't seem to set the sensitivity state on buttons in web pages:
dogtail.config.config.ensureSensitivity=False

wb = EpiphanyApp()

# Browse to Google front page
tab = wb.browseToUrl("http://www.google.com")
#tab.dump()

gfp = GoogleFrontPage(tab)

# Debug dump:
#gfp.child(roleName='text').dump()

# Do a search:
gfp.textEntry.text = "dogtail"
print gfp.searchButton.actions
gfp.searchButton.actions['press'].do()

sleep(2)

# Scrape out the results:
frame = gfp.child(roleName='frame')
results = frame.findChildren(predicate.GenericPredicate(roleName='text'), recursive=False)
for result in results:
    print "Result:"
    print result.text
    print "--------------------------------------"
