#!/usr/bin/env python
__author__ = "David Malcolm <dmalcolm@redhat.com>"

from distutils.core import setup
from distutils.command.bdist_rpm import bdist_rpm

setup (
        name = 'dogtail-appwrappers',
        version = '0.0.1',
        description = """Helper wrappers for writing Dogtail scripts for specific applications.""",
        author = """David Malcolm <dmalcolm@redhat.com>""",
        author_email = 'dogtail-list@gnome.org',
        url = 'http://people.redhat.com/zcerza/dogtail/',
        packages = ['dogtail.appwrappers'],
        cmdclass = {
                'bdist_rpm': bdist_rpm
                }
)

# vim: sw=4 ts=4 sts=4 noet ai
