# -*- coding: UTF-8 -*-
"""
Wrappers to ease writing scripts for specific applications

Author: David Malcolm <dmalcolm@redhat.com>
"""

__author__ = "David Malcolm <dmalcolm@redhat.com>"
__version__ = "0.5.3"
__copyright__ = "Copyright © 2005 Red Hat, Inc."
__license__ = "GPL"
