from dogtail.wrapped import Application

class Gedit(Application):
    def setText(self, text):
        buf = self.child(roleName = 'text')
        buf.text = text

    def getText(self):
        return self.child(roleName='text').text

    def openLocation(self, uri):
        menu = self.menu("File")
        menu.click()
        menu.menuItem("Open Location...").click()
        dlg = self.dialog('Open Location')
        dlg.child(roleName = 'text').text = uri
        dlg.button('Open').click()

    def saveAs(self, uri):
        menu = self.menu("File")
        menu.click()
        menu.menuItem("Save As...").click()
        dlg = self.dialog('Save As...')
        dlg.child(roleName = 'text').text = uri
        dlg.button('Save').click()

    def printPreview(self):
        menu = self.menu("File")
        menu.click()
        menu.menuItem("Print Preview")
