Summary: Wrapper scripts to ease writing Dogtail tests for specific applications
Name: dogtail-appwrappers
Version: 0.0.1
Release: 1%{?dist}
License: GPL
Group: User Interface/X
URL: http://people.redhat.com/zcerza/dogtail/
Source0: http://people.redhat.com/zcerza/dogtail/releases/dogtail-appwrappers-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python
Requires: dogtail

%description
Wrapper scripts to ease writing Dogtail tests for specific applications

%prep
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%setup -q

%build
python ./setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python ./setup.py install -O2 --root=$RPM_BUILD_ROOT --record=%{name}.files
rm -rf $RPM_BUILD_ROOT/%{_docdir}/dogtail-appwrappers

%post

%postun

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{python_sitelib}/dogtail/
%doc COPYING
%doc README

%changelog
* Wed Sep 13 2006 David Malcolm <dmalcolm@redhat.com> - 0.0.1-1
- Initial packaging

