#!/usr/bin/env python

import dogtail.tree
from dogtail.apps.wrappers.gcalctool import *
import dogtail.utils

dogtail.utils.run('gcalctool')
gcalctool = GCalcTool(dogtail.tree.root.application('gcalctool'))

import dogtail.config
dogtail.config.config.defaultDelay = 0.3

for i in range(1, 12):
	for j in range(1, 12):
		result = gcalctool.doProduct(i, j)
		print ("python says: %s x %s = %s , gcalctool says: %s"%(i, j, i*j, result))
		if int(result)!=(i*j):
			raise "DodgyMathsError"
