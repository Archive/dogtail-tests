#!/usr/bin/env python
# Dogtail demo script
__author__ = 'David Malcolm <dmalcolm@redhat.com>'


# Test configuring an IMAP and SMTP account
#
# Assumes evolution is configured and is running

from dogtail.appwrappers.evolution import *

account = MixedAccount(fullName="John Doe",
                       emailAddress="jdoe@example.com",
                       receiveMethod = IMAPSettings(server="mail.example.com",
                                                    username="jdoe",
                                                    useSecureConnection=UseSecureConnection.ALWAYS,
                                                    authenticationType="password"),
                       sendMethod = SMTPSettings(server="smtp.example.com",
                                                 useSecureConnection=UseSecureConnection.NEVER))

evo = doFirstTimeWizard(account, "test IMAP/SMTP account", "America/New_York")
