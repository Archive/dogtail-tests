#!/usr/bin/env python

# Dogtail testcase for gedit, based on
# list at http://live.gnome.org/Gedit/TestCases

# Author: David Malcolm <dmalcolm@redhat.com>

import unittest

import dogtail.tree
try: from dogtail.appwrappers.gedit import Gedit
except ImportError: from appwrappers.gedit import Gedit
from dogtail.utils import run

import os.path
from stat import *

def makeGnomeVfsUri(path):
    return "file://"+os.path.abspath(path)

def editWithGedit(filePath):
    # Manipulate with gedit:
    run('gedit')
    gedit = Gedit(dogtail.tree.root.application('gedit'))
    gedit.openLocation(makeGnomeVfsUri(filePath))
    gedit.setText('bar')
    menu = gedit.menu('File')
    menu.click()
    menu.menuItem('Save').click()
    menu.click()
    menu.menuItem('Quit').click()

def createTestFile(filePath):
    # Create test file

    # Remove existing file and backup, if present:
    try:
        os.remove(filePath)
    except OSError: pass
    try:
        os.remove('%s~'%filePath)
    except OSError: pass
    
    outfile = open(filePath, 'wb')
    outfile.write('foo')
    outfile.close()


class GeditSavingTests(unittest.TestCase):
    def checkBackupFile(self, filePath, expectedContent):
        backupFile = open('%s~'%filePath, 'rb')
        self.assertEqual(backupFile.read(), expectedContent)

    def test1(self):
        """
        create a file, set its contents to "foo", open it in gedit,
        set the contents to "bar", save it, check the contents is "bar",
        check if the backup file is created and if it properly contains "foo"
        """
        filePath = 'test1.txt'

        createTestFile(filePath)
        editWithGedit(filePath)

        # Checking:
        self.checkBackupFile(filePath, 'foo')
        infile = open(filePath, 'rb')
        self.assertEqual(infile.read(),'bar\x0a')


    def test3(self):
        """
        like 1, but before opening chmod the file (in a way that it is still writable), upon saving check perms are preserved
        """
        filePath = 'test3.txt'

        createTestFile(filePath)

        os.chmod(filePath, 0777)
        
        editWithGedit(filePath)        

        # Checking:
        self.checkBackupFile(filePath, 'foo')
        infile = open(filePath, 'rb')
        self.assertEqual(infile.read(),'bar\x0a')

        # Check mode was preserved:
        mode = os.stat(filePath)[ST_MODE]
        self.assertEquals(S_IMODE(mode), 0777)
    
if __name__=="__main__":
    unittest.main()

